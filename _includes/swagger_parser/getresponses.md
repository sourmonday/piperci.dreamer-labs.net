
{% assign hasparams = false %}
{% for response in site.data.swagger[page.swaggerfile]paths[page.swaggerkey][page.method]responses %}
{% assign hasparams = true %}
{% endfor %}
{% if hasparams == true %}
  <table>
    <thead>
      <tr><th>Code</th><th>Returns</th></tr>
    </thead>
    {% for response in site.data.swagger[page.swaggerfile]paths[page.swaggerkey][page.method]responses %}
      {% if response[0] == '200'%}
        {% if include.many == "yes" %}
          <tr>
            <td>{{ response[0] }}</td>
            <td>List of <a href="{{include.path}}"> {{include.model}} </a></td>
          </tr>
        {% else %}
          <tr>
            <td>{{ response[0] }}</td>
            <td><a href="{{include.path}}"> {{include.model}} </a></td>
          </tr>
        {% endif %}
      {% else %}
        <tr>
          <td>{{ response[0] }}</td>
          <td>{{ response[1].description }}</td>
        </tr>
      {% endif %}
    {% endfor %}
  </table>
{% else %}
None
{% endif %}

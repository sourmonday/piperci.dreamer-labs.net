---
title: PUT /task/{task_id}
sidebar: mydoc_sidebar
permalink: put_task.html
folder: piperci
swaggerfile: swagger
swaggerkey: /task/{task_id}
method: put
---
## Description
{% include swagger_parser/getattribute.md attribute="description" %}

## Body parameters
{% include swagger_parser/getrequestbody.md %}

## Examples
<pre>
<code>{
  "message": "some message",
  "status": "delegated"
}
</code>
</pre>

## Responses
{% include swagger_parser/getresponses.md path="/api_task_event.html" model= "Task Event"%}

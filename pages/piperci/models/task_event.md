---
title: Task Event
sidebar: mydoc_sidebar
permalink: api_task_event.html
folder: piperci
swaggerfile: swagger
swaggerkey: TaskEvent
---
## Task event attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}

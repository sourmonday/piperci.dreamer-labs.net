---
title: Task Event Header
sidebar: mydoc_sidebar
permalink: api_task_event_header.html
folder: piperci
swaggerfile: swagger
swaggerkey: task_event_header
---
## Header attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}

---
title: Artifact
sidebar: mydoc_sidebar
permalink: api_artifact.html
folder: piperci
swaggerfile: swagger
swaggerkey: Artifact
---
## Artifact attributes
{% include swagger_parser/getmodel.md %}

{% include links.html %}

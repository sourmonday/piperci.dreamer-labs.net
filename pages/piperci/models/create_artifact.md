---
title: Create Artifact
sidebar: mydoc_sidebar
permalink: api_create_artifact.html
folder: piperci
swaggerfile: swagger
swaggerkey: CreateArtifact
---
## Create artifact attributes
{% include swagger_parser/getmodel.md required="yes" %}

{% include links.html %}

---
title: Create Task Event
sidebar: mydoc_sidebar
permalink: api_create_task_event.html
folder: piperci
swaggerfile: swagger
swaggerkey: CreateEvent
---
## Create task event attributes
{% include swagger_parser/getmodel.md required="yes" %}

{% include links.html %}
